# Docker Compose for Horse Race Simulator

docker-compose configuration for Horse Race Simulator.

You need to execute migrations and load fixtures within docker after mysql started.

`docker exec docker_backend_1 php symfony/bin/console doctrine:migrations:migrate -q`

`docker exec docker_backend_1 php symfony/bin/console doctrine:fixtures:load -q`
